<?php
    class apartment extends rents{
        public $kitchen = true;

        public function __construct($title, $type, $address, $price, $description, $kitchen){
            parent::__construct($title, $type, $address, $price, $description);
            $this->kitchen = $kitchen;
        }
        public function getSummaryLine(){
            if ($this->kitchen === true) {
                return parent::getSummaryLine() . ' ' . ', have kitchen';
            }else {
                return parent::getSummaryLine() . ' ' . ', don\'t have kitchen';
            }
        }
    }
?>