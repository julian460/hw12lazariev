<?php
class rents{
    public $title = "Название";
    public $type = "Тип объекта";
    public $address = "Адрес объекта";
    public $price = "Цена за ренту";
    public $description = "Описание Объекта";

    public function __construct($title, $type, $address, $price, $description){
        $this->title = $title;
        $this->type = $type;
        $this->address = $address;
        $this->price = $price;
        $this->description = $description;
    }

    public function getSummaryLine(){
        return $this->title . ', ' . $this->type . ', ' . $this->address . ', ' . $this->price;
    }
}    
?>