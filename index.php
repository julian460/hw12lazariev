<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/classes/rents.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/classes/hotel.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/classes/apartment.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/classes/house.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/classes/htmlRents.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/array.php';
$rentsObjects = [];
foreach ($rents as $value) {
    if ($value['type'] === 'hotel_room') {
        $rentsObjects[] = new hotel($value['title'],
        $value['type'], 
        $value['address'], 
        $value['price'], 
        $value['description'], 
        $value['roomNumber']);
    }elseif ($value['type'] === 'apartment') {
        $rentsObjects[] = new apartment($value['title'],
        $value['type'], 
        $value['address'], 
        $value['price'], 
        $value['description'], 
        $value['kitchen']);
    }else {
        $rentsObjects[] = new house($value['title'],
        $value['type'], 
        $value['address'], 
        $value['price'], 
        $value['description'], 
        $value['roomsAmount']);
    }
}
$rents = new HtmlRents();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Couple of variants for rent">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <title>Rent</title>
</head>
<body>
<div class="content">
    <div class="row d-flex justify-content-center">
        <div class="col-6 ">
            <h1>Availeble places for rent</h1>
            <ul class="list-group">
                <?php foreach($rentsObjects as $rent):?>
                    <li class="list-group-item">
                        <?=$rent->getSummaryLine()?>
                    </li>
                <?php endforeach;?>
            </ul>
        </div>
        <div class="row">
        <h2>All objects</h2>
        <?php foreach($rentsObjects as $value):?>
            <?=$rents->write($value)?>
        <?php endforeach;?>
    </div>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
</body>
</html>